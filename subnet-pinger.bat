@echo off
echo Lets ping.
pause
echo Day: %DATE%
echo Time: %TIME%
echo Ping...
for /L %%x IN (1,1,255) DO @ping 192.168.1.%%x -n 1 -w 10 | find "TTL"
echo Ping end.
@echo.
echo List ARP Table.
cmd /k arp -a
@pause
