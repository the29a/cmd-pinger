@echo off
echo Gateways ping.
pause
echo Day: %DATE% 
echo Time: %TIME%
ipconfig | findstr "192.168"
echo Ping...
for /L %%x IN (1,1,255) DO @ping 192.168.%%x.1 -n 1 -w 10 | findstr "TTL="
echo Ping end.
@echo.
@pause
